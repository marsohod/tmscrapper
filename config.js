require('dotenv').config();

const config = {
    telegram: {
        id: process.env.TELEGRAM_APP_ID,
        hash: process.env.TELEGRAM_APP_HASH,
        phone: process.env.TELEGRAM_PHONE_NUMBER,
        storage: process.env.TELEGRAM_FILE,
        devServer: false,
        msgHistory: {
            maxMsg: 100,
            limit: 100,
        },
        getChat: {
            limit: 200
        },
    },
    dbfile: process.env.DB_FILE,
    chatdb: process.env.CHAR_FILE,
    server: process.env.SERVER_URL,
    chatlist: process.env.CHAT_LIST,
    storageUsers: process.env.STORAGEUSERS,
}

module.exports = config;