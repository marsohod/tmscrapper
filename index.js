
const { getChat, chatHistory, lastActiveUsers } = require('./chat-history')
const db = require('./utils/db');
const {checkLogin} = require('./utils/node-storage');

const run = async (chat) => {
  await lastActiveUsers(chat)
}

const beforeStart = async () => {
  await checkLogin();

  let chatlist = await db.getChatList();

  console.log('Acitve chats:', chatlist.join(', '));

  for ( let i in chatlist ) {
    await start( chatlist[i] );
  }
}

const start = async ( chatname ) => {
  let chat = await db.getChat(chatname);

  if (!chat) {
    chat = await getChat(chatname);
    await db.updateChat(chat)
  }

  return new Promise(resolve => {
    let timerId = setTimeout(function tick() {
      run(chat);
      timerId = setTimeout(tick, 360000);
      resolve(true);
    }, 3000);
  });
}

beforeStart();