const config    = require('../config');
const fs        = require("fs");
const DBFILE    = config.dbfile;
const CHATDB    = config.chatdb;
const CHATLIST  = config.chatlist;
const SU        = config.storageUsers;

async function readDb(file) {
    return new Promise((res, rej) => {
        fs.readFile(file, 'utf8', (err, data) => {
            err ? rej(err) : res(data)
        });
    })
}

async function writeFile(json, file) {
    return new Promise((res, rej) => {
        fs.writeFile(file, JSON.stringify(json, null, 2), "utf8", (err) => {
            err ? rej(err) : res();
        });
    })
}

async function writeChannelUsers(json, channelname) {
    return new Promise((res, rej) => {
        fs.writeFile(SU + channelname + '.json', JSON.stringify(json, null, 2), "utf8", (err) => {
            err ? rej(err) : res();
        });
    })
}

async function readChannelUsers(channelname) {
    try {
        const readFile = await readDb(SU + channelname + '.json');
        return JSON.parse(readFile);
    } catch (err) {
        return [];
    }
}

async function updateLastMsgId(num) {
    try {
        const work = await writeFile({
            lastMsgId: num
        }, DBFILE)
    } catch (err) {
        console.log("error, couldnt save to file " + err)
    }
}

async function getLastMsgId() {
    try {
        const readFile = await readDb(DBFILE);
        const file = JSON.parse(readFile)
        return file.lastMsgId;
    } catch (err) {
        console.log("file not found so making a empty one and adding default value " + err)
        await updateLastMsgId(1);
        return 1;
    }
}

async function getChat( chatname ) {
    try {
        const readFile = await readDb(CHATDB);
        const file = JSON.parse(readFile)

        for ( let i in file ) {
            if ( file[i].username == chatname ) {
                return file[i];
            }
        }

        return false;
    } catch (err) {
        console.log(err);
        return false;
    }
}

async function getChats( chatname ) {
    try {
        const readFile = await readDb(CHATDB);
        return JSON.parse(readFile);
    } catch (err) {
        console.log(err);
        return [];
    }
}

async function updateChat (obj) {
    let chats = await getChats();

    chats.push(obj); 
    chats = uniqueArray(chats, 'id');

    try {
        const work = await writeFile(chats, CHATDB)
    } catch (err) {
        console.log("error, couldnt save chat to file " + err)
    }
}

async function getChatList () {
    try {
        const readFile  = await readDb(CHATLIST);
        const file      = JSON.parse(readFile);
        return file.list;
    } catch (err) {
        console.log('getChatList', err);
        return false;
    }
}

const uniqueArray = function(myArr, prop) {
    return myArr.filter((obj, pos, arr) => {
        return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
    });
}

module.exports = {
    updateLastMsgId,
    getLastMsgId,
    getChat,
    getChats,
    updateChat,
    getChatList,
    writeChannelUsers,
    readChannelUsers
}