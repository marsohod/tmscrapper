<?php	
	$groups 	= json_decode(file_get_contents("../storage/chatlist.json"), true);
	
	sort($groups['list'], SORT_NATURAL | SORT_FLAG_CASE);
	
	if ( isset($_GET['group']) ) 	$groupName 	= $_GET['group'];
	if ( isset($groupName) ) 		$users 		= json_decode(file_get_contents("../storage/users/". $groupName .".json"), true);

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Page Title</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="/assets/css/reset.css">
	<link rel="stylesheet" href="/assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="/assets/css/style.css">
</head>
<body>
	<?php if ( !isset($groupName) ) {
		$totalWithUsername 	= 0;
		$total 				= 0;
		$items 				= [];
		$itemsWithUsername	= [];
		
		foreach( $groups['list'] as $key => $item ) {
			$items[$item] 				= json_decode(file_get_contents("../storage/users/". $item .".json"), true);
			$itemsWithUsername[$item]	= [];
			
			if ( isset($items) ) {
				$itemsWithUsername[$item] = array_filter( $items[$item], function ($v) { return isset($v['username']) ? 1 : 0; } );
			}

			$totalWithUsername += count($itemsWithUsername[$item]);
			$total += count($items[$item]);
		}
	?>
		<div class="container">
			<div class='row'>
				<div class="col-12">
					<h1>Groups</h1>
					<table class='table'>
						<tr>
							<th>Group</th>
							<th>With username (<?=$totalWithUsername?>)</th>
							<th>Total (<?=$total?>)</th>
						<tr>
						<?php
							foreach( $groups['list'] as $key => $item ) {
								print '<tr>
									<td><a href="/?group='. $item .'">' . $item . '</a></td>
									<td>'. count($itemsWithUsername[$item]).'</td>
									<td>'. count($items[$item]).'</td>
									</tr>';
							}
						?>
					</ul>
				</div>
			</div>
		</div>
	<?php } else { ?>
		<table class='table'>
			<tr>
				<th>Дата</th>
				<th>Id</th>
				<th>Username</th>
				<th>Name</th>
				<th>Channel</th>
			<tr>
			<?php
				foreach( $users as $key => $user ) {
					$date = date_parse($user['date']);

					print '<tr>
						<th>'. $date['day'] . '.' . $date['month'] . '.' . $date['year'] .'</th>
						<th>'. $user['id'] .'</th>
						<th>'. (isset($user['username'])
							? '<a href="https://t.me/'. $user['username'] . '" target="blank_">'. $user['username'] . '</a>'
							: '') . '</th>
						<th>'. ( isset($user['first_name'])
							? $user['first_name']
							: '') . ( isset($user['last_name'])
								? ' ' . $user['last_name']
								: '' ) .'</th>
						<th><a href="https://t.me/'. $groupName .'" target="blank_">'. $groupName .'</a></th>
					<tr>';
				}
			?>
		</table>
	<?php } ?>
</body>
</html>